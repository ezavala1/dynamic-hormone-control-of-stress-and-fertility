a = load('physiological.mat');
b = load('hypercortisolism.mat')
c = load('physiological_stressor.mat')
d = load('Breen_OVX_E2implant.mat')


subplot(121)
plot(a.K_C, a.cl, 'k', 'linewidth',1.5);hold on;
ylim([0, 6])
yyaxis right
plot(b.K_C, 100*(-a.cl+b.cl)./a.cl, 'r', 'linewidth',1.5);
ylim([0, 100]);
xlim([a.K_C(1), a.K_C(end)])
set(gca,'fontsize',20);

subplot(122)
plot(c.K_C, 100*(-c.cl+c.cl2)./c.cl2, 'linewidth',1.5);hold on;
plot(d.K_C, 100*(-d.cl+d.cl2)./d.cl2, 'linewidth',1.5);hold on;
ylim([0, 100]);
xlim([c.K_C(1), c.K_C(end)])
set(gca,'fontsize',20);


set(gcf, 'paperposition',[0 0 16*2 10])
print('-depsc', '-r300',['model_results.eps'])
