# Dynamic hormone control of stress and fertility

Codes associated to the paper https://www.frontiersin.org/articles/10.3389/fphys.2020.598845/full

The model equations were numerically solved and analyzed in MATLAB R2020a using ode45 routines. Details of the mathematical model development and parameter values are described in the Supplementary Material of the paper.
