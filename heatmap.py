#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 10:06:54 2020

@author: Admin
"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import seaborn as sns

#sns.set()

#%% Settings

# Select whether you're working on workstation (Admin) or laptop (ederzavala)
comp = 'Admin'
#comp = 'ederzavala'

# Change path and import data
parent = '/Users/'+comp+'/Dropbox/Project - HPA & HPG axis/models/model_v23'
os.chdir(parent)

# Load data
#file = 'freq_matrix_physio_1hr.csv'
file = 'freq_matrix_physio_2hr.csv'
#file = 'freq_matrix_hypercort_1hr.csv'
#file = 'freq_matrix_hypercort_2hr.csv'
data = pd.read_csv(file, sep=',', header=None)

freq = (np.transpose(data))*60./np.pi # Freq pulses/hr

#%% Plot Heatmap

fig1 = plt.figure(1, facecolor='white')
plt.clf()

plt.rcParams.update({'font.size': 15})

ax = plt.gca()

ax = sns.heatmap(freq, cmap="OrRd", cbar_kws={'label': 'PG frequency post-stressor \n (pulses/hr)'})

xticks = np.arange(len(freq.columns))
ticks_x = ticker.FuncFormatter(lambda xticks, pos: '{0:g}'.format(xticks*30/1440))
ax.xaxis.set_major_formatter(ticks_x)
ax.xaxis.set_major_locator(plt.MultipleLocator(1440/30))
plt.xticks(rotation=0)

yticks = np.arange(len(freq))
ticks_y = ticker.FuncFormatter(lambda yticks, pos: '{0:g}'.format(yticks/100))
ax.yaxis.set_major_formatter(ticks_y)
ax.yaxis.set_major_locator(plt.MultipleLocator(20))
ax.set_ylim(0,200)

ax.set_xlabel('Time (days)')
ax.set_ylabel('Stress amplitude')

#ax.set_title('Physiological CORT - 1hr')
ax.set_title('Physiological CORT - 2hr')
#ax.set_title('Hypercortisolism - 1hr')
#ax.set_title('Hypercortisolism - 2hr')

fig1.tight_layout()
plt.show()

#%% Save figures

#fig1.savefig('freq_post-stressor_physio_1hr.png',bbox_inches='tight')
fig1.savefig('freq_post-stressor_physio_2hr.png',bbox_inches='tight')
#fig1.savefig('freq_post-stressor_hypercort_1hr.png',bbox_inches='tight')
#fig1.savefig('freq_post-stressor_hypercort_2hr.png',bbox_inches='tight')
