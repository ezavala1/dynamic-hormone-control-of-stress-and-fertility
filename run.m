%% To be run using MATLAB_R2020a

% Select whether to save figs: True=1 / False=0
% (the saved PDF looks better than the display in the figure panel)
save_figures = 0;

%% Select case to simulate

casename = 'physiological'
% casename = 'OVX'
% casename = 'OVX_E2implant'
% casename = 'Breen_OVX'
% casename = 'Breen_OVX_E2implant'
% casename = 'hypercortisolism'
% casename = 'physiological_stressor'
% casename = 'physiological_stressor_responsecurve'

% casename = 'hypercortisolism_stressor'
% casename = 'OVX_stressor'
% casename = 'OVX_E2implant_stressor'
% casename = 'hypercortisolism_stressor_responsecurve'
% casename = 'omega_pg_sensitivity' % set number of days high enough (such that at least 3 cycles are simulated)

%% Set runtime parameters 

nd = 5; % number of days to run

% Stress pulse parameters (needed for c_as = 1)
ptimes = 24*1; % time at pulse (in hours)
plength = pi/12; % square pulse duration (in radians) (pi/12 corresponds to 2 hours)
pA = 1; % pulse amplitude

% CORT pellet parameters (needed for the Breen experiments, i.e., c_hc = 2)
pellet_start = 2*pi;
pellet_end = 4*pi;
pC = 1.7; % to adjust the CORT pellet magnitude

% Parameters of coupling function (PG frequency on Estrous frequency) 
omega_PG_mu = 0.1;
omega_PG_spread = 0.055;

% Default case pars (corresponds to 'physiological')
c_as = 0;  % acute stressor
c_hc = 0;  % hypercortisolism (1 = hypercortisolism, 2 = CORT pellet)
c_ovx = 0; % OVX
c_E2i = 0; % E2 implant
phase_response = 0;
omega_pg_sensitivity=0;

% Cases
if strcmp(casename,'physiological')
    c_hc = 0;              
elseif strcmp(casename,'hypercortisolism')
    c_hc = 1;         
elseif strcmp(casename,'physiological_stressor')
    c_as = 1;
elseif strcmp(casename,'hypercortisolism_stressor')
    c_hc = 1; c_as = 1;
elseif strcmp(casename,'OVX')
    c_ovx = 1;
elseif strcmp(casename,'OVX_E2implant')
    c_ovx = 1; c_E2i = 1;
elseif strcmp(casename,'OVX_stressor')
    c_ovx = 1; c_as = 1;
elseif strcmp(casename,'OVX_E2implant_stressor')
    c_ovx = 1; c_E2i = 1; c_as = 1;
elseif strcmp(casename,'Breen_OVX')
    c_hc = 2; c_ovx = 1;
elseif strcmp(casename,'Breen_OVX_E2implant')
    c_hc = 2; c_ovx = 1; c_E2i = 1;
elseif strcmp(casename,'physiological_stressor_responsecurve')      
    c_as = 1; phase_response = 1; 
elseif strcmp(casename,'hypercortisolism_stressor_responsecurve')  
    c_as = 1; c_hc = 1; phase_response = 1;
elseif strcmp(casename,'omega_pg_sensitivity') 
    omega_pg_sensitivity = 1;
%     omega_PG_mu = [0.01,0.1,0.15]; 
%     omega_PG_spread = [0.055,0.06,0.1];
    omega_PG_mu = [0.01:0.01:0.2]; 
    omega_PG_spread = [0.01:0.01:0.2];
else
    error('Selected case does not exist. Please set casename to a valid option.')
end
 
%% Run Simulation

% Standard simulation
if phase_response == 0 &&  omega_pg_sensitivity == 0
    ptime_h = ptimes; % onset of acute stressor in hours
    ptime = ptime_h * pi/24; % onset of acute stressor as phase 
    ptimeend = ptime + plength; 

    % Run the solver
    params.Tspan = 0:0.1:(24*60*nd);    % run time
    X0=[0; 0; 0; 0; 0; 0; 0; 0; 0; 0.5];   % initial conditions

    opt = odeset('MaxStep',1);  % solver settings
    [T, Y] = ode45(@(t,y) model_HPA_HPG(t,y,params,c_hc,c_as,c_ovx,c_E2i,ptime,ptimeend,pA,pellet_start,pellet_end,pC,omega_PG_mu,omega_PG_spread), params.Tspan , X0, opt);    % solver
    
% Phase response simulation
elseif phase_response == 1 &&  omega_pg_sensitivity == 0
    ptimes = 0:0.5:24*nd-2; % Shifts acute stressor onset in 0.5 h steps from time 0 to 2 hours before end of simulation 
    freq_vector = zeros(length(ptimes),1);
    amp_vector = zeros(length(ptimes),1);
    for time_i = 1:length(ptimes)
        ptime_h = ptimes(time_i); % onset of acute stressor in hours
        ptime = ptime_h * pi/24; % onset of acute stressor as phase 
        ptimeend = ptime + plength; 

        % Run the solver
        params.Tspan = 0:0.1:(24*60*nd);    % run time
        X0=[0; 0; 0; 0; 0; 0; 0; 0; 0; 0.5];   % initial conditions

        opt = odeset('MaxStep',1);  % solver settings
        [T, Y] = ode45(@(t,y) model_HPA_HPG(t,y,params,c_hc,c_as,c_ovx,c_E2i,ptime,ptimeend,pA,pellet_start,pellet_end,pC,omega_PG_mu,omega_PG_spread), params.Tspan , X0, opt);    % solver

        freq_vector(time_i) = mean(Y((ptime_h*60*10+1):(ptime_h*60*10+2*60*10),7)); % save average pulse generator frequency over 2h stressor 
        amp_vector(time_i) = max(Y((ptime_h*60*10+1):(ptime_h*60*10+2*60*10),2)); % save max CORT amplitude over 2h stressor 
%         freq_vector(time_i) = Y(ptime_h*60*10+150,7); % save average pulse generator frequency at 15 min post stressor 
%         amp_vector(time_i) = Y(ptime_h*60*10+150,2); % save average pulse generator amplitude at 15 min post stressor     
   
    end
elseif phase_response == 0  &&  omega_pg_sensitivity == 1
    sensitivity_matrix = zeros(length(omega_PG_spread)*length(omega_PG_mu),3);
    iii=1;
    for  i = 1:length(omega_PG_mu)    
        i
        for j = 1:length(omega_PG_spread)
                    
            ptime_h = ptimes; % onset of acute stressor in hours
            ptime = ptime_h * pi/24; % onset of acute stressor as phase 
            ptimeend = ptime + plength; 

            % Run the solver
            params.Tspan = 0:0.1:(24*60*nd);    % run time
            X0=[0; 0; 0; 0; 0; 0; 0; 0; 0; 0.5];   % initial conditions

            opt = odeset('MaxStep',1);  % solver settings
            [T, Y] = ode45(@(t,y) model_HPA_HPG(t,y,params,c_hc,c_as,c_ovx,c_E2i,ptime,ptimeend,pA,pellet_start,pellet_end,pC,omega_PG_mu(i),omega_PG_spread(j)), params.Tspan , X0, opt);    % solver
             
            % Compute cycle length 
            [pks,locs] = findpeaks(Y(:,5));
            try
                cycle_length=(locs(3)-locs(2))/(24*60*10);
            catch exception
                cycle_length=999; % if simulated period is too short to resolve cycle length   
            end
                            
            sensitivity_matrix(iii,1)=omega_PG_mu(i);
            sensitivity_matrix(iii,2)=omega_PG_spread(j);
            sensitivity_matrix(iii,3)=cycle_length;
            iii=iii+1;
        end
    end
    save('cycle_length.mat','sensitivity_matrix')
else   
    error('Please set phase_response and omega_pg_sensitivity to a valid combination, i.e., either both =0 or one =1 and the other =0') 
end   

%% Set Plot Parameters

% Time period to plot [days from start of simulation]
xmin = 0;
xmax = nd;
 
% Display
set(0, 'DefaultAxesFontName', 'Arial')
fs = 12;    % fontsize
lw = 0.5;     % linewidth

%% Figure 1: Plot oscillator variables (phases, amplitudes, and frequencies)

figure(1); clf(1);
t = tiledlayout(8,1,'Padding','none','TileSpacing','none');
t.Units = 'inches';
t.OuterPosition = [0 0 5 6.1];

vars = ["\phi_{CORT}","A_{CORT}","\phi_H","\phi_E","A_E","\phi_{PG}","\omega_{PG}","A_{PG}", "filtered cort"];
state_variables_plot = [1:7, 9];

for ii = 1:length(state_variables_plot)
    nexttile;
    plot(T/(24*60),Y(:,state_variables_plot(ii)), 'linewidth', lw); set(gca,'fontsize',fs);
    ylabel(vars(ii),'FontSize',fs,'FontWeight','bold');
    xticklabels({})
    xlim([xmin xmax])
    hold on; 
end
 
xticks('auto'); xticklabels('auto');
xlabel('Time (days)','FontSize',fs,'FontWeight','bold');

if save_figures == 1 && phase_response == 0
    exportgraphics(t,strcat('state_vars_',casename,'.pdf'),'ContentType','vector','BackgroundColor','w')
end

%% Figure 2: Plot stress and reproductive variables

figure(2); clf(2);
t = tiledlayout(2,1,'Padding','none','TileSpacing','none');
t.Units = 'inches';
t.OuterPosition = [0 0 5 3];

% Stress axis
nexttile;
if c_hc == 0
    plot(T/(24*60),Y(:,2) .* sin(Y(:,1)).^2, 'linewidth', lw, 'color', '#D95319'); set(gca,'fontsize',fs);
elseif c_hc == 1
%     plot(T/(24*60),Y(:,2) .* sin(Y(:,1)).^2, 'linewidth', lw, 'color', '#D95319'); set(gca,'fontsize',fs);
    plot(T/(24*60),0.8 + (Y(:,2) - 0.8) .* sin(Y(:,1)).^2, 'linewidth', lw, 'color', '#D95319'); set(gca,'fontsize',fs);
elseif c_hc == 2
    cort = Y(:,2) .* sin(Y(:,1)).^2;
    cort(pellet_start/pi*60*24*10:pellet_end/pi*60*24*10) = pC;
    plot(T/(24*60),cort, 'linewidth', lw, 'color', '#D95319'); set(gca,'fontsize',fs);
end
xticklabels({})
% legend('CORT','Location','SouthEast','FontSize',6)
ylabel({'CORT levels';'(normalised)'},'FontSize',fs);
xlim([xmin xmax])
if c_hc == 0 && c_as == 0
%     ylim([0 1.1]);
    ylim([0 3]);
elseif c_hc == 1 && c_as == 1
    ylim([0 3.5]);
elseif c_hc == 2 || c_as == 1
    ylim([0 2]);
else
    ylim([0 3]);
end

% Reproductive axis
nexttile;
sigma = 0.5;
f_E =  @(x) ( sin(x - sigma * sin(x).^2).^2 );
plot(T/(24*60),sin(Y(:,6)).^2, 'linewidth', lw, 'color', '#0072BD'); set(gca,'fontsize',fs); hold on;   % Pulse generator
ylabel({'PG activity';'(normalised)'},'FontSize',fs);
yyaxis right
plot(T/(24*60),Y(:,7)*60/pi, 'linewidth', 1, 'color', '#D95319'); set(gca,'fontsize',fs);    % PG frequency
ylabel({'PG frequency';'(pulses/hr)'},'FontSize',fs);
ylim([0 2.3])
yyaxis left
if c_ovx == 0
    h1=plot(T/(24*60),f_E(Y(:,4)), 'linewidth', 1, 'color', 'k'); set(gca,'fontsize',fs);
%     legend('Pulse generator','Reproductive cycle','PG frequency','Location','SouthEast','FontSize',6)
    legend(h1,'Estrous cycle','Location','SouthEast','FontSize',6)
elseif c_ovx == 1
%     legend('Pulse generator','PG frequency','Location','SouthEast','FontSize',6)
end
xlim([xmin xmax])
ylim([0 1])

xlabel('Time (days)','FontSize',fs);

if save_figures == 1 && phase_response == 0
    exportgraphics(t,strcat('endo_axes_',casename,'.pdf'),'ContentType','vector','BackgroundColor','w')
end
 
%% Figure 3: Plot phase response
 
if phase_response == 1
    figure(3); clf(3);
    t = tiledlayout(1,1,'Padding','none','TileSpacing','none');
    t.Units = 'inches';
    t.OuterPosition = [0 0 5 3];
    nexttile;
    colororder({'k','#D95319'})

    % PG frequency
    plot(T/(24*60),Y(:,7)*60/pi,'linewidth',1,'color','k'); set(gca,'fontsize',fs);
    hold on
    
    % PG frequency following stressor
    h2=plot((ptimes+0.25)/24,freq_vector*60/pi,'linewidth',1,'color','#0072BD'); set(gca,'fontsize',fs); % plot frequency 15 min after onset of stressor
    ylabel({'PG frequency';'(pulses/hr)'},'FontSize',fs);
    ylim([0 2.5])
    
    % CORT amplitude following stressor
    yyaxis right
    plot((ptimes+0.25)/24,amp_vector,'linewidth',1,'color','#D95319'); set(gca,'fontsize',fs); % plot amplitude 15 min after onset of stressor    
    plt = gca;
    plt.YAxis(2).Color = '#D95319';
    ylabel('\DeltaCORT','FontSize',fs);    
    ylim([0.8 4])

%     legend('{\it f}_{PG} no stress','{\it f}_{PG} post stressor','\DeltaCORT', 'Location', 'NorthOutside','Orientation','Horizontal','Box','Off','FontSize',8)
%     legend(h2,'PG frequency post-stressor', 'Location', 'NorthEast','Orientation','Horizontal','Box','Off','FontSize',8)
    legend(h2,'PG frequency fold change', 'Location', 'NorthEast','Orientation','Horizontal','Box','Off','FontSize',8)
    xlim([xmin xmax-0.5])
    xlabel('Time (days)','FontSize',fs);

    if save_figures == 1
        exportgraphics(t,strcat('phase_resp_',casename,'.pdf'),'ContentType','vector','BackgroundColor','w')
    end    
end

% Optional: Plot cycle length map 
if omega_pg_sensitivity == 1   
    figure(3)
    Z = reshape(sensitivity_matrix(:,3),length(omega_PG_mu),length(omega_PG_spread));
    imagesc(omega_PG_mu,omega_PG_spread,Z)
    %mesh(omega_PG_mu,omega_PG_spread,Z,'FaceColor','interp')
    colorbar
    caxis([3 25])
    xlabel('omega PG mu')
    ylabel('omega PG spread')
    
    if save_figures == 1
        x_width=7.25 ;y_width=5;
        set(gcf,'color','w','PaperUnits','inches','PaperPosition',[0 0 x_width y_width],'PaperSize',[x_width y_width]);
        set(gcf,'color','w');
        print(gcf, strcat('omega_pg_sens_',casename,'.pdf'), '-dpdf', '-fillpage')
    end     
end
