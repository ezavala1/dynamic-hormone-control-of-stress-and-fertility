%% Model

% Used normalized hormone concentrations by squaring sines
function dx = model_HPA_HPG(t,x,params,c_hc,c_as,c_ovx,c_E2i,ptime,plength,pA,pellet_start,pellet_end,pC,omega_PG_mu,omega_PG_spread)

% Natural frequencies
omega_C_0 = 1*pi/75;          % ultradian oscillation, CORT (T = 75 min)
omega_H_0 = 1*pi/(24*60);     % circadian oscillation, Hypothalamus (T = 24 hr)
omega_E_0 = 1*pi/(24*60*3);   % estrous oscillation, Estradiol (T = 3 days; lower boundary, shortest cycle length observed)
omega_PG_0 = 1*pi/30;         % maximum pulse generator frequency (T = 30 min)

% Auxiliary parameters
sigma = 0.5; % Estrous cycle skewness
K_E = 0.025;  % CORT amplitude sensitivity to the estrous cycle amplitude. Gonadectomy effects below this threshold.
K_C = 1.7;  % PG frequency sensitivity to CORT amplitude (see figure_KC.pdf for the effects of this parameter value)
K_S = 0.6;  % PG frequency sensitivity to stress pulse
n = 4;  % Hill coefficient 
loE = 0.02; % low E2 levels following OVX
hiE = 0.98; % hight E2 levels following E2 implant
off_hc = 0.8; % Offset hypercortisolism

% Auxiliary functions
unit_step = @(x) (x >= 0);  % step function (discrete Heaviside)

% Acute stressor
if c_as == 1
    pulse = pA * unit_step(x(3)-ptime) * unit_step(plength-x(3));    % square shaped
elseif c_as == 0
    pulse = 0;
end

% Coupling functions (incl. different experimental conditions)
if c_hc == 0  % Phase-dependent (phi_H) circadian amplitude modulation of CORT
    f_H_1 = sin(x(3))^2;      % Normal CORT     
elseif c_hc == 1
    f_H_1 = off_hc + 2*off_hc*sin(x(3))^2;  % Hypercortisolism
%     omega_C_0 = 1*pi/53.33;          % ultradian oscillation in hyper-CORT (T = 80 min)
    omega_C_0 = 1*pi/50;          % ultradian oscillation in hyper-CORT (T = 80 min)
elseif c_hc == 2    % Breen experiment (must have c_ovx == 1)
    f_H_1 = sin(x(3))^2;
end

% Filtered CORT levels
if c_hc == 0
    cort_level = x(2) .* sin(x(1)).^2;    
elseif (c_hc == 2 && x(3) >= pellet_start && x(3) <= pellet_end)  % Induced by pellet
    cort_level = pC;
else % Hypercortisolism (c_hc == 1)
%     cort_level = x(2) .* sin(x(1)).^2;
    cort_level = off_hc + (x(2) - off_hc) .* sin(x(1)).^2;
end

f_E = @(x) ( sin(x - sigma * sin(x).^2).^2 );                                       % Phase-dependent (phi_E) estrous phase modulation of PG amplitude
f_PG = exp(-((x(7))-(omega_PG_mu)).^2/(2*omega_PG_spread.^2));                      % Frequency-dependent (omega_PG) PG frequency modulation of estrous phase

% Function emulating the frequency dependency of the NKB and Dyn on the frequency of the KNDy pulse generator
Xtmp  = (x(9) - 0.5*x(8)) ;
Ytmp  = (- 0.5*x(9) + x(8));
H     = 50;
K     = 1/H * (1 + 13*pulse^n / ((K_S)^n + pulse^n) + 6*x(10)^n / (K_C^n + x(10)^n) );
f_E_3 = exp(H*(Xtmp-K))./(exp(H*(Xtmp-K))+1) .* exp(H*(Ytmp-K))./(exp(H*(Ytmp-K))+1);

%% rhs of model equations

% CORT
dx(1,1) = omega_C_0 - pulse*0.05/pA ;   % phi_CORT + Stress pulse
dx(2,1) = pulse + f_H_1 * (x(5)^n / (K_E^n + x(5)^n) ) - x(2);   % A_CORT + Stress pulse

% Hypothalamic drive
dx(3,1) = omega_H_0;   % phi_Hypothalamus

% Estrous cycle
dx(4,1) = omega_E_0 * f_PG; % phi_Estrous

if c_ovx == 0 && c_E2i == 0
    f_E_2 =  f_E(x(4));
    dx(5,1) = 0.1 + 0.9*f_E(x(4)) - x(5);  % A_Estrous
elseif c_ovx == 1 && c_E2i == 0
    f_E_2 =  0.99; % OVX: set f_E to max, argmax(f_E(x)) \approx 5*pi/8 %TODO set to number!
    dx(5,1) = loE - x(5); % A_Estrous + OVX: low levels of Estrogen corresponds to Diestrous
elseif c_ovx == 1 && c_E2i == 1
    f_E_2 =  0.2; % OVX + implant
    dx(5,1) = hiE - x(5);   % A_Estrous + OVX + implant: high levels of Estrogen corresponding to Proestrous
end

% Pulse generator (PG)
dx(6,1) =  x(7);  % phi_PG
dx(7,1) =  f_E_3 * omega_PG_0  - x(7); % omega_PG

dx(8,1) =  f_E_2 - x(8); %NKB equation
dx(9,1)  = f_E_2 - x(9); %Dyn equation

dx(10,1) = 0.005*(cort_level - x(10));  % cort_level_smoothed
end